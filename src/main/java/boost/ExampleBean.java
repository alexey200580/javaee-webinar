package boost;

import javax.annotation.PostConstruct;
import javax.inject.Named;

@Named(value = "firstBean")
public class ExampleBean {

    private String name;

    @PostConstruct
    private void initialize() {
        name="Hello world";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void someAction() {
        name = "";
    }
}
